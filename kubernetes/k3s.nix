{ config, pkgs, ... }:
{
  services.k3s = {
    enable = true;
    role = "server";
    token = "8c91762d04544434500210cfbfd6cd55";
    clusterInit = true;
    extraFlags = "--bind-address=10.10.4.43 --disable servicelb --disable traefik --tls-san 10.10.4.43 --node-ip=10.10.4.43 --node-external-ip=10.10.4.43 --flannel-iface=ens18";
  };
}
