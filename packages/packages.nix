# programs.nix
{ config, pkgs, ... }:

{
  # List packages installed in system
  environment.systemPackages = with pkgs; [
    vim
    wget
    git
    neovim
  ];
}
