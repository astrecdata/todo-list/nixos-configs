# neovim.nix
{ config, programs, pkgs, ...}:

{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    configure = {
      customRC = ''
        set number
        syntax on
        set list
        colorscheme habamax
      '';
    };
  };
}
