# ssh.nix

{ config, ... }:

{
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "yes";	
  };
}
